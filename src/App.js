import "./App.css";
import RoutesApp from "./Config/Routes/Routes";

function App() {
  return (
    <div>
      <RoutesApp />
    </div>
  );
}

export default App;
