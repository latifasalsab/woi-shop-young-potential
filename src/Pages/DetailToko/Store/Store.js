import React from "react";
import { makeStyles } from "@material-ui/core";

import img from "../../../Assets/img/logo.jpg";

import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import FavoriteIcon from "@material-ui/icons/Favorite";

const useStyles = makeStyles(() => ({
  Container: {
    padding: "78px 16px 20px 16px !important",
  },
  Img: {
    height: "74px !important",
    width: "74px !important",
  },
  Title: {
    fontWeight: "600 !important",
    fontSize: "14px !important",
    lineHeight: "21px !important",
    margin: "0 0 4px 0 !important",
  },
  SubTitle: {
    fontWeight: "400 !important",
    fontSize: "12px !important",
    lineHeight: "18px !important",
    margin: "0 0 8px 0 !important",
  },
  WrapperChips: {
    gap: "8px !important",
  },
  Chips: {
    padding: "4px 0 !important",
    gap: "10px !important",
    fontSize: "8px !important",
    fontWeight: "600 !important",
    lineHeight: "12px !important",
    height: "20px !important",
    fontFamily: "'Rubik' !important",
  },
}));

export default function Store() {
  const classes = useStyles();
  return (
    <Grid container className={classes.Container}>
      <Grid item xs={3} sx={{ position: "relative" }}>
        <img src={img} alt="logo" className={classes.Img} />
        <Box
          style={{
            position: "absolute",
            bottom: "0",
            right: "25px",
            padding: "4px 8px",
            fontSize: "12px",
            fontWeight: "600",
            lineHeight: "18px",
            backgroundColor: "#03AC0E",
            borderRadius: "100px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <FavoriteIcon
            style={{
              width: "14px",
              height: "14px",
              color: "#fff",
              marginRight: "4px",
            }}
          />
          5.0
        </Box>
      </Grid>
      <Grid item xs={9}>
        <Box>
          <p className={classes.Title}>Toko Semesta</p>
          <p className={classes.SubTitle}>
            Jl. Kasipah Raya No. 182, Jatingaleh, Kec. Candisari, Kota Semarang
          </p>
        </Box>
        <Box className={classes.WrapperChips}>
          <Chip
            label="Express"
            className={classes.Chips}
            style={{ backgroundColor: "#049C0E", color: "#fff" }}
          />
          <Chip
            label="09.00"
            className={classes.Chips}
            style={{
              backgroundColor: "#03AC0E38",
              color: "#03AC0E",
              margin: "0 8px",
            }}
          />
          <Chip
            label="16.00"
            className={classes.Chips}
            style={{ backgroundColor: "#03AC0E38", color: "#03AC0E" }}
          />
        </Box>
      </Grid>
    </Grid>
  );
}
