import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core";
import axios from "axios";

import Header from "../../Component/Header/Header";
import Store from "./Store/Store";
import Category from "../../Component/Category/Category";

import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";

const useStyles = makeStyles(() => ({
  Divider: {
    backgroundColor: "#F5F5F5",
    width: "100%",
    height: "4px",
  },
  Chip: {
    position: "fixed",
    bottom: "24px",
    left: "44%",
    backgroundColor: "#03AC0E !important",
    padding: "8px 11px !important",
    borderRadius: "100px !important",
    fontSize: "12px !important",
    fontWeight: "400 !important",
    lineHeight: "18px !important",
    color: "#fff !important",
    fontFamily: "'Rubik' !important",
    boxShadow: "0px 8px 16px 0px #03AC0E3D",
    cursor: "pointer !important",
  },
}));

export default function DetailToko() {
  const classes = useStyles();

  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const { data: response } = await axios.get(
          "https://dummyjson.com/products/categories"
        );
        setData(response);
      } catch (error) {
        console.error(error.message);
      }
      setLoading(false);
    };

    fetchData();
  }, []);
  return (
    <Box style={{ position: "relative" }}>
      <Header link="/" title="Detail Toko" />
      <Store />
      <Box className={classes.Divider}></Box>
      {loading && <div>loading...</div>}
      {!loading && <Category list={data} />}
      <Chip label="Jadikan Toko Favorit" className={classes.Chip} />
    </Box>
  );
}
