import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core";
import { useParams } from "react-router-dom";
import axios from "axios";

import Header from "../../Component/Header/Header";
import SearchBar from "../../Component/SearchBar/SearchBar";
import ProductMerchant from "../../Component/ProductMerchant/ProductMerchant";

import Box from "@mui/material/Box";

const useStyles = makeStyles(() => ({
  Content: {
    padding: "54px 0 0 0",
  },
}));

export default function ListProduct() {
  const classes = useStyles();

  const { category } = useParams();

  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const { data: response } = await axios.get(
          `https://dummyjson.com/products/category/${category}`
        );
        setData(response.products);
      } catch (error) {
        console.error(error.message);
      }
      setLoading(false);
    };

    fetchData();
  }, []);
  return (
    <Box>
      <Header link="/detail-toko" title={category} />
      <Box className={classes.Content}>
        <SearchBar placeholder={category} />
        {loading && <div>loading...</div>}
        {!loading && <ProductMerchant list={data} />}
      </Box>
    </Box>
  );
}
