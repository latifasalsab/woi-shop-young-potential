import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import axios from "axios";

import ProductDetailList from "../../Component/ProductDetail/ProductDetail";

import Box from "@mui/material/Box";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const useStyles = makeStyles(() => ({
  Appbar: {
    height: "54px !important",
    minWidth: "375px",
    padding: "15px 16px 15px 16px !important",
    gap: "10px !important",
    boxShadow: "0px 2px 8px 0px #0000000A",
    position: "fixed",
    zIndex: "1000",
    backgroundColor: "#fff",
  },
  Back: {
    height: "24px !important",
    display: "flex !important",
    justifyContent: "space-between !important",
    gap: "16px !important",
  },
  Left: {
    display: "flex !important",
    gap: "16px !important",
    alignItems: "center",
  },
  IconArrow: {
    color: "#03AC0E",
    cursor: "pointer",
  },
  Title: {
    fontWeight: "600 !important",
    lineHeight: "24px !important",
  },
}));

export default function ProductDetail() {
  const classes = useStyles();

  let navigate = useNavigate();

  const { detail } = useParams();

  const [data, setData] = useState([]);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const { data: response } = await axios.get(
          `https://dummyjson.com/products/${detail}`
        );
        setData(response);
      } catch (error) {
        console.error(error.message);
      }
      setLoading(false);
    };

    fetchData();
  }, []);
  return (
    <Box>
      <Box className={classes.Appbar}>
        <Box className={classes.Back}>
          <Box className={classes.Left}>
            <Box onClick={() => navigate(-1)}>
              <ArrowBackIcon className={classes.IconArrow} />
            </Box>
            <span className={classes.Title}>{data.title}</span>
          </Box>
        </Box>
      </Box>
      {loading && <div>loading...</div>}
      {!loading && <ProductDetailList item={data} />}
    </Box>
  );
}
