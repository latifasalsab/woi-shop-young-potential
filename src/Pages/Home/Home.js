import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";

const useStyles = makeStyles(() => ({
  Container: {
    width: "100%",
    height: "100vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  Chip: {
    backgroundColor: "#03AC0E !important",
    padding: "8px 11px !important",
    borderRadius: "100px !important",
    fontSize: "12px !important",
    fontWeight: "400 !important",
    lineHeight: "18px !important",
    color: "#fff !important",
    fontFamily: "'Rubik' !important",
    boxShadow: "0px 8px 16px 0px #03AC0E3D",
    cursor: "pointer !important",
  },
}));

export default function Home() {
  const classes = useStyles();
  return (
    <Box className={classes.Container}>
      <Link to="/detail-toko">
        <Chip label="Detail Toko" className={classes.Chip} />
      </Link>
    </Box>
  );
}
