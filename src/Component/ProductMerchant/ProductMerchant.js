import React from "react";
import ListItem from "./ListItem/ListItem";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";

const useStyles = makeStyles(() => ({
  Container: {
    display: "flex",
    flexWrap: "wrap",
    width: "100% !important",
    margin: "0 16px !important",
    gap: "14px",
  },
}));

export default function ProductMerchant({ list }) {
  const classes = useStyles();

  return (
    <Box className={classes.Container}>
      {list.map(item => (
        <ListItem key={item.id} item={item} link={`${item.id}`} />
      ))}
    </Box>
  );
}
