import React from "react";
import { makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const useStyles = makeStyles(() => ({
  Product: {
    gap: "16px",
    width: "107px !important",
  },
  Img: {
    width: "104px",
    height: "104px",
  },
  WrapperTitle: {
    height: "36px",
    margin: "16px 0 !important",
  },
  Title: {
    fontSize: "12px",
    fontWeight: "400",
    lineHeight: "18px",
    color: "#292929",
  },
  Price: {
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
    color: "#292929",
  },
  Button: {
    backgroundColor: "#FAFAFA !important",
    border: "1px solid #03AC0E !important",
    borderRadius: "100px !important",
    padding: "8px 16px !important",
    fontSie: "14px !important",
    fontWeight: "600 !important",
    lineHeight: "21px !important",
    textTransform: "none !important",
    color: "#03AC0E !important",
    margin: "16px 0 !important",
    width: "100% !important",
  },
}));

export default function ListItem({ item: { title, price, thumbnail }, link }) {
  const classes = useStyles();
  return (
    <Link className={classes.Product} to={link}>
      <Box className={classes.Image}>
        <img
          src={thumbnail}
          alt="product"
          style={{ width: "104px", height: "104px", objectFit: "cover" }}
        />
      </Box>
      <Box className={classes.Wrapper}>
        <Box className={classes.WrapperTitle}>
          <p className={classes.Title}>{title}</p>
        </Box>

        <p className={classes.Price}>${price}</p>
        <Button className={classes.Button}>Tambah</Button>
      </Box>
    </Link>
  );
}
