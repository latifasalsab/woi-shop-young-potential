import React from "react";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";
import Chip from "@mui/material/Chip";
import Button from "@mui/material/Button";

const useStyles = makeStyles(() => ({
  Content: {
    padding: "54px 16px 0 16px",
  },
  Img: {
    width: "343px",
    height: "343px",
    objectFit: "cover",
    margin: "16px 0 0 0 !important",
  },
  Category: {
    color: "#7A7A7A !important",
    fontWeight: "600 !important",
    fontSize: "12px !important",
    lineHeight: "18px !important",
    borderRadius: "100px !important",
    padding: "4px 12px !important",
    margin: "16px 0 0 0 !important",
  },
  Title: {
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
    color: "#292929",
    margin: "12px 0 0 0",
  },
  Price: {
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
    color: "#292929",
    margin: "4px 0 0 0",
  },
  Divider: {
    width: "100%",
    height: "1px",
    backgroundColor: "#EBEBEB",
    margin: "24px 0 0 0",
  },
  DescriptionTitle: {
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
    color: "#292929",
    margin: "24px 0 0 0",
  },
  Description: {
    fontSize: "12px",
    fontWeight: "400",
    lineHeight: "18px",
    color: "#7A7A7A",
    margin: "6px 0 0 0",
  },
  Button: {
    backgroundColor: "#03AC0E !important",
    borderRadius: "100px !important",
    width: "100% !important",
    color: "#fff !important",
    fontSize: "14px !important",
    fontWeight: "600 !important",
    lineHeight: "21px !important",
    textTransform: "none !important",
    padding: "16px 24px !important",
    margin: "36px 0 16px 0 !important",
  },
}));

export default function ProductDetailList({
  item: { thumbnail, images, category, title, price, description },
}) {
  const classes = useStyles();
  return (
    <Box className={classes.Content}>
      <img src={thumbnail} alt="thumbnail" className={classes.Img} />
      <Chip label={category} className={classes.Category} />
      <p className={classes.Title}>{title}</p>
      <p className={classes.Price}>${price}</p>
      <Box className={classes.Divider}></Box>
      <p className={classes.DescriptionTitle}>Deskripsi Produk</p>
      <p className={classes.Description}>{description}</p>
      <Button variant="contained" className={classes.Button}>
        Tambah Pesanan
      </Button>
    </Box>
  );
}
