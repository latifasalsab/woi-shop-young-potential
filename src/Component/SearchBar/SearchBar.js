import React from "react";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";

import SearchIcon from "@material-ui/icons/Search";
import FilterAltIcon from "@mui/icons-material/FilterAlt";

const useStyles = makeStyles(() => ({
  Container: {
    margin: "16px 0 !important",
    padding: "8px 16px !important",
    display: "flex",
  },
  Input: {
    width: "290px !important",
    gap: "8px !important",
    // padding: "12px 16px",
    borderRadius: "100px !important",
    fontSize: "14px !important",
    fontWeight: "400 !important",
    lineHeight: "16.59px !important",
    color: "#7A7A7A !important",
    backgroundColor: "#FAFAFA",
    borderColor: "#EBEBEB !important",
  },
  Icon: {
    width: "20px !important",
    height: "20px !important",
    fill: "#7A7A7A !important",
  },
  WrapperFilter: {
    backgroundColor: "#03AC0E",
    borderRadius: "100px",
    width: "41px",
    height: "41px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "12px !important",
  },
}));

export default function SearchBar({ placeholder }) {
  const classes = useStyles();
  return (
    <Box className={classes.Container}>
      <OutlinedInput
        required
        placeholder={placeholder}
        className={classes.Input}
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon className={classes.Icon} />
          </InputAdornment>
        }
      />
      <Box className={classes.WrapperFilter}>
        <FilterAltIcon style={{ color: "#fff" }} />
      </Box>
    </Box>
  );
}
