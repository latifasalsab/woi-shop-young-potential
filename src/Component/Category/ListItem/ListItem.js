import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

import smartphone from "../../../Assets/img/smartphone.png";
import laptop from "../../../Assets/img/laptop.png";
import fragrances from "../../../Assets/img/fragrances.png";
import skincare from "../../../Assets/img/skincare.png";
import groceries from "../../../Assets/img/groceries.png";
import homeDecoration from "../../../Assets/img/home-decoration.png";
import furniture from "../../../Assets/img/furniture.png";
import tops from "../../../Assets/img/tops.png";
import womenDress from "../../../Assets/img/women-dress.png";
import womenShoes from "../../../Assets/img/women-shoes.png";
import tshirts from "../../../Assets/img/tshirt.png";
import manShoes from "../../../Assets/img/man-shoes.png";
import manWatches from "../../../Assets/img/man-watch.png";
import womenWatches from "../../../Assets/img/women-watch.png";
import womenBags from "../../../Assets/img/women-bags.png";
import womenJawellery from "../../../Assets/img/women-jewellery.png";
import sunglasses from "../../../Assets/img/sunglasses.png";
import automotive from "../../../Assets/img/automotive.png";
import motorcycle from "../../../Assets/img/motorcycle.png";
import lighting from "../../../Assets/img/lighting.png";

const useStyles = makeStyles(() => ({
  Category: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
  },
  Img: {
    width: "40px",
    height: "40px",
    margin: "auto",
  },
  Menu: {
    width: "73px",
  },
  WrapImg: {
    width: "73px",
    display: "flex",
    alignitems: "center",
    justifyItems: "center",
  },
  MenuText: {
    fontSize: "12px",
    fontWeight: "400",
    lineHeight: "18px",
    textAlign: "center",
    color: "#7A7A7A",
  },
}));

export default function ListItem({ category, link }) {
  const classes = useStyles();
  return (
    <Link className={classes.Menu} to={link}>
      <div className={classes.WrapImg}>
        {category === "laptops" ? (
          <img src={laptop} alt="laptops" className={classes.Img} />
        ) : category === "smartphones" ? (
          <img src={smartphone} alt="smartphone" className={classes.Img} />
        ) : category === "fragrances" ? (
          <img src={fragrances} alt="fragrances" className={classes.Img} />
        ) : category === "skincare" ? (
          <img src={skincare} alt="skincare" className={classes.Img} />
        ) : category === "groceries" ? (
          <img src={groceries} alt="groceries" className={classes.Img} />
        ) : category === "home-decoration" ? (
          <img
            src={homeDecoration}
            alt="home-decoration"
            className={classes.Img}
          />
        ) : category === "furniture" ? (
          <img src={furniture} alt="furniture" className={classes.Img} />
        ) : category === "tops" ? (
          <img src={tops} alt="tops" className={classes.Img} />
        ) : category === "womens-dresses" ? (
          <img src={womenDress} alt="womens-dress" className={classes.Img} />
        ) : category === "womens-shoes" ? (
          <img src={womenShoes} alt="womens-shoes" className={classes.Img} />
        ) : category === "mens-shirts" ? (
          <img src={tshirts} alt="mens-shirts" className={classes.Img} />
        ) : category === "mens-shoes" ? (
          <img src={manShoes} alt="mens-shoes" className={classes.Img} />
        ) : category === "mens-watches" ? (
          <img src={manWatches} alt="mens-watches" className={classes.Img} />
        ) : category === "womens-watches" ? (
          <img
            src={womenWatches}
            alt="womens-watches"
            className={classes.Img}
          />
        ) : category === "womens-bags" ? (
          <img src={womenBags} alt="womens-bags" className={classes.Img} />
        ) : category === "womens-jewellery" ? (
          <img
            src={womenJawellery}
            alt="womens-jawellery"
            className={classes.Img}
          />
        ) : category === "sunglasses" ? (
          <img src={sunglasses} alt="sunglasses" className={classes.Img} />
        ) : category === "automotive" ? (
          <img src={automotive} alt="automotive" className={classes.Img} />
        ) : category === "motorcycle" ? (
          <img src={motorcycle} alt="motorcycle" className={classes.Img} />
        ) : (
          <img src={lighting} alt="lighting" className={classes.Img} />
        )}
      </div>

      <div style={{ height: "36px", margin: "12px 0 0 0" }}>
        <p className={classes.MenuText}>{category}</p>
      </div>
    </Link>
  );
}
