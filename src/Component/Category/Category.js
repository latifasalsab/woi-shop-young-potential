import React from "react";
import ListItem from "./ListItem/ListItem";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";

const useStyles = makeStyles(() => ({
  Container: {
    padding: "20px 16px !important",
    width: "100% !important",
  },
  Category: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    gap: "16px",
  },
  Title: {
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "24px",
    margin: "0 0 19px 0",
  },
}));

export default function Category({ list }) {
  const classes = useStyles();
  return (
    <Box className={classes.Container}>
      <p className={classes.Title}>Kategori Produk</p>
      <Box className={classes.Category}>
        {list.map((item, index) => (
          <ListItem key={index} category={item} link={`${item}`} />
        ))}
      </Box>
    </Box>
  );
}
