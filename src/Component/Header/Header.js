import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

import Box from "@mui/material/Box";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles(() => ({
  Appbar: {
    height: "54px !important",
    minWidth: "375px",
    padding: "15px 16px 15px 16px !important",
    gap: "10px !important",
    boxShadow: "0px 2px 8px 0px #0000000A",
    position: "fixed",
    zIndex: "1000",
    backgroundColor: "#fff",
  },
  Back: {
    height: "24px !important",
    display: "flex !important",
    justifyContent: "space-between !important",
    gap: "16px !important",
  },
  Left: {
    display: "flex !important",
    gap: "16px !important",
    alignItems: "center",
  },
  IconArrow: {
    color: "#03AC0E",
  },
  Title: {
    fontWeight: "600 !important",
    lineHeight: "24px !important",
  },
}));

export default function Header({ link, title }) {
  const classes = useStyles();
  return (
    <Box className={classes.Appbar}>
      <Box className={classes.Back}>
        <Box className={classes.Left}>
          <Link to={link}>
            <ArrowBackIcon className={classes.IconArrow} />
          </Link>
          <span className={classes.Title}>{title}</span>
        </Box>
        {/* <SearchIcon /> */}
      </Box>
    </Box>
  );
}
