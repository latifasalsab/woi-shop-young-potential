import React from "react";
// import { makeStyles } from "@material-ui/core";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "../../Pages/Home/Home";
import DetailToko from "../../Pages/DetailToko/DetailToko";
import ListProduct from "../../Pages/ListProduct/ListProduct";
import ProductDetail from "../../Pages/ProductDetail/ProductDetail";

import Box from "@mui/material/Box";

// const useStyles = makeStyles(() => ({
//   Container: {
//     "&@media (min-width: 600px)"
//   }
// }));

export default function RoutesApp() {
  // const classes = useStyles();
  return (
    <Box
      sx={{
        maxWidth: "375px",
        backgroundColor: "#fff",
        minHeight: "100vh",
        margin: "auto",
        padding: "0",
      }}
    >
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/detail-toko" element={<DetailToko />} />
          <Route path="/detail-toko/:category" element={<ListProduct />} />
          <Route
            path="/detail-toko/:category/:detail"
            element={<ProductDetail />}
          />
        </Routes>
      </Router>
    </Box>
  );
}
